GPU implementation of HOG algorithm developed by DLIB library.

For further details refer to document https://gitlab.com/daniele.adriano/gpu-prj/-/blob/master/performance/gpu-paper.pdf