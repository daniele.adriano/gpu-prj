#pragma once

#include <stdio.h>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "common.h"

#define CHECK(call)                                                            \
{                                                                              \
    const cudaError_t error = call;                                            \
    if (error != cudaSuccess)                                                  \
    {                                                                          \
        fprintf(stderr, "Error: %s:%d, ", __FILE__, __LINE__);                 \
        fprintf(stderr, "code: %d, reason: %s\n", error,                       \
                cudaGetErrorString(error));                                    \
    }                                                                          \
}

inline void device_warmup()
{
	auto start = current_time();
	unsigned char *warmup;
	CHECK(cudaMalloc((void**)&warmup, sizeof(unsigned char)));
	CHECK(cudaFree(warmup));
	printf("Device warmup, time %f\n", elapsed_time(start));
}