#pragma once

#include <chrono>
#include <ctime>    

using namespace std;

inline chrono::system_clock::time_point current_time()
{
	return chrono::system_clock::now();
}

inline double elapsed_time(chrono::system_clock::time_point start)
{
	std::chrono::duration<double> elapsed_seconds = current_time() - start;
	return elapsed_seconds.count();
}

inline bool double_equals(double a, double b, double epsilon = 1e-15)
{
	return fabs(a - b) < epsilon;
}
