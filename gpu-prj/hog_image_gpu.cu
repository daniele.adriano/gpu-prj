#pragma once

#include <stdio.h>
#include <dlib/image_keypoint.h>
#include <math_constants.h>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "cuda_common.cu"
#include "hog_image_gpu.h"

using namespace dlib;
using namespace std;

#define WARP_SIZE 32
#define _9BINS 9
#define _8CELLS 8


//*********************************************************************************************
// GRAYSCALE image
//*********************************************************************************************

__global__ void load_kernel_grayscale_basic(unsigned char *dev_image, double* src_hist_cells, double* dst_hist_cells, const unsigned long cell_size, const unsigned long num_orientation_bins, const int gradient_type, const unsigned long image_nc, const unsigned long cell_nc);
__global__ void load_kernel_grayscale_shared_hist(unsigned char *dev_image, double* dst_hist_cells, const unsigned long cell_size, const unsigned long num_orientation_bins, const int gradient_type, const unsigned long image_nc, const unsigned long cell_nc);
__global__ void load_kernel_grayscale_shared_hist_opt(unsigned char *dev_image, double* dst_hist_cells, const unsigned long cell_size, const unsigned long num_orientation_bins, const int gradient_type, const unsigned long image_nc, const unsigned long cell_nc);
__global__ void load_kernel_grayscale_9bins_shared_hist_opt(unsigned char *dev_image, double* dst_hist_cells, const unsigned long cell_size, const int gradient_type, const unsigned long image_nc, const unsigned long cell_nc);
__global__ void load_kernel_grayscale_8cells_9bins_shared_hist_opt(unsigned char *dev_image, double* dst_hist_cells, const int gradient_type, const unsigned long image_nc, const unsigned long cell_nc);
__global__ void load_kernel_grayscale_shared_img(unsigned char *dev_image, double* src_hist_cells, double* dst_hist_cells, const unsigned long cell_size, const unsigned long num_orientation_bins, const int gradient_type, const unsigned long image_nc, const unsigned long cell_nc);

void load_image_gpu(const array2d<unsigned char>& image, const unsigned long cell_size, const unsigned long num_orientation_bins, const int gradient_type, const unsigned int cell_nr, const unsigned int cell_nc, double* hist_cells, int kernel_mode)
{
	// Each thread handles one pixel in order to calculate a single gradient
	// Each block handles gradients of one cell in order to create the histogram related to one cell
	unsigned int cuda_block_size = cell_size * cell_size;
	unsigned int cuda_grid_size = cell_nr * cell_nc;

	// Grayscale image, 1 byte for each pixel (unsigned char)
	unsigned long image_size = image.size() * sizeof(unsigned char);		

	// Image pointer on device memory
	unsigned char *dev_image;	
	CHECK(cudaMalloc((void**) &dev_image, image_size));
	CHECK(cudaMemcpy(dev_image, image.begin(), image_size, cudaMemcpyHostToDevice));

	// Histogram destination area, one histogram for each cuda block (cell)
	double *dev_dst_hist_cells;
	unsigned long dst_hist_cells_size = cuda_grid_size * num_orientation_bins * sizeof(double);
	CHECK(cudaMalloc((void**)&dev_dst_hist_cells, dst_hist_cells_size));

	switch (kernel_mode)
	{
	case kernel_mode_basic:
	{
		unsigned long src_hist_cells_size = cuda_grid_size * cuda_block_size * num_orientation_bins * sizeof(double);

		// Histogram working area, one histogram for each thread
		double *dev_src_hist_cells;
		CHECK(cudaMalloc((void**)&dev_src_hist_cells, src_hist_cells_size));
		CHECK(cudaMemset(dev_src_hist_cells, 0, src_hist_cells_size));

		// Calculate cells histograms, no shared memory is used
		load_kernel_grayscale_basic << <cuda_grid_size, cuda_block_size >> > (dev_image, dev_src_hist_cells, dev_dst_hist_cells, cell_size, num_orientation_bins, gradient_type, image.nc(), cell_nc);
		CHECK(cudaDeviceSynchronize());
		CHECK(cudaFree(dev_src_hist_cells));
	}
	break;

	case kernel_mode_shared_hist:
	{
		// Histogram working area for each block, one histogram for each thread
		unsigned long shared_mem_size = cuda_block_size * num_orientation_bins * sizeof(double);

		// Calculate cells histograms, uses shared memory instead of histogram working area (dev_src_hist_cells pointer)
		load_kernel_grayscale_shared_hist << <cuda_grid_size, cuda_block_size, shared_mem_size >> > (dev_image, dev_dst_hist_cells, cell_size, num_orientation_bins, gradient_type, image.nc(), cell_nc);
		CHECK(cudaDeviceSynchronize());
	}
	break;

	case kernel_mode_shared_hist_opt:
	{
		// Histogram working area for each block, one histogram for each thread
		unsigned long shared_mem_size = cuda_block_size * num_orientation_bins * sizeof(double);

		// Calculate cells histograms, uses shared memory instead of histogram working area (dev_src_hist_cells pointer)
		// This version avoid shared memory bank conflicts: thread 0 histogram is stored into bank0, bank32, bank64, etc; thread 1 histogram is store into bank1, bank33, bank65, etc.; and so on.
		load_kernel_grayscale_shared_hist_opt << <cuda_grid_size, cuda_block_size, shared_mem_size >> > (dev_image, dev_dst_hist_cells, cell_size, num_orientation_bins, gradient_type, image.nc(), cell_nc);
		CHECK(cudaDeviceSynchronize());
	}
	break;

	case kernel_mode_9bins_shared_hist_opt:
	{		
		// Histogram working area for each block, one histogram for each thread
		unsigned long shared_mem_size = cuda_block_size * num_orientation_bins * sizeof(double);

		// Calculate cells histograms, uses shared memory instead of histogram working area (dev_src_hist_cells pointer)
		// This version avoid shared memory bank conflicts: thread 0 histogram is stored into bank0, bank32, bank64, etc; thread 1 histogram is store into bank1, bank33, bank65, etc.; and so on.
		// Avoid for statements to handle dynamic num_orientation_bins values; also performs warp unrolling on parallel reduction.
		load_kernel_grayscale_9bins_shared_hist_opt << <cuda_grid_size, cuda_block_size, shared_mem_size >> > (dev_image, dev_dst_hist_cells, cell_size, gradient_type, image.nc(), cell_nc);
		CHECK(cudaDeviceSynchronize());
	}
	break;

	case kernel_mode_8cells_9bins_shared_hist_opt:
	{
		// Calculate cells histograms, uses shared memory instead of histogram working area (dev_src_hist_cells pointer)
		// This version avoid shared memory bank conflicts: thread 0 histogram is stored into bank0, bank32, bank64, etc; thread 1 histogram is store into bank1, bank33, bank65, etc.; and so on.
		// Avoid for statements to handle dynamic num_orientation_bins values; also performs warp unrolling on parallel reduction.
		// Avoid to handle dynamic cell_size values, use static instead of dynamic shared memory
		load_kernel_grayscale_8cells_9bins_shared_hist_opt << <cuda_grid_size, cuda_block_size >> > (dev_image, dev_dst_hist_cells, gradient_type, image.nc(), cell_nc);
		CHECK(cudaDeviceSynchronize());
	}
	break;

	case kernel_mode_shared_img:
	{
		unsigned long src_hist_cells_size = cuda_grid_size * cuda_block_size * num_orientation_bins * sizeof(double);

		// Histogram working area, one histogram for each thread
		double *dev_src_hist_cells;
		CHECK(cudaMalloc((void**)&dev_src_hist_cells, src_hist_cells_size));
		CHECK(cudaMemset(dev_src_hist_cells, 0, src_hist_cells_size));

		// Image patch size for each block, 2 is used for top/bottom rows, left/right columns
		unsigned long shared_mem_size = (cell_size + 2) * (cell_size + 2) * sizeof(unsigned char);

		// Calculate cells istograms, uses shared memory to store the image patch related to current block
		load_kernel_grayscale_shared_img << <cuda_grid_size, cuda_block_size, shared_mem_size >> > (dev_image, dev_src_hist_cells, dev_dst_hist_cells, cell_size, num_orientation_bins, gradient_type, image.nc(), cell_nc);
		CHECK(cudaDeviceSynchronize());
		CHECK(cudaFree(dev_src_hist_cells));
	}
	break;

	}

	// Copy result to host memory
	CHECK(cudaMemcpy(hist_cells, dev_dst_hist_cells, dst_hist_cells_size, cudaMemcpyDeviceToHost));

	// Free device memory	
	CHECK(cudaFree(dev_dst_hist_cells));
	CHECK(cudaFree(dev_image));
}



//*********************************************************************************************
// RGB_PIXEL image
//*********************************************************************************************

__global__ void load_kernel_rgb_basic(rgb_pixel *devImg, double* src_hist_cells, double* dst_hist_cells, const unsigned long cell_size, const unsigned long num_orientation_bins, const int gradient_type, const unsigned long image_nc, const unsigned long cell_nc);

void load_image_gpu(const array2d<rgb_pixel>& image, unsigned long cell_size, const unsigned long num_orientation_bins, const int gradient_type, const unsigned int cell_nr, const unsigned int cell_nc, double* hist_cells, int kernel_mode)
{

	// Each thread handles one pixel in order to calculate a single gradient
	// Each block handles gradients of one cell in order to create the histogram related to one cell
	unsigned int cuda_block_size = cell_size * cell_size;
	unsigned int cuda_grid_size = cell_nr * cell_nc;

	// RGB image, 3 bytes for each pixel (3 x unsigned char)
	unsigned long image_size = sizeof(rgb_pixel) * image.size();

	// Image pointer on device memory
	rgb_pixel *dev_image;
	CHECK(cudaMalloc((void**)&dev_image, image_size));
	CHECK(cudaMemcpy(dev_image, image.begin(), image_size, cudaMemcpyHostToDevice));

	// Histogram destination area, one histogram for each cuda block (cell)
	double *dev_dst_hist_cells;
	unsigned long dst_hist_cells_size = cuda_grid_size * num_orientation_bins * sizeof(double);
	CHECK(cudaMalloc((void**)&dev_dst_hist_cells, dst_hist_cells_size));

	// Histogram working area, one histogram for each thread
	unsigned long src_hist_cells_size = cuda_grid_size * cuda_block_size * num_orientation_bins * sizeof(double);
	double *dev_src_hist_cells;
	CHECK(cudaMalloc((void**)&dev_src_hist_cells, src_hist_cells_size));
	CHECK(cudaMemset(dev_src_hist_cells, 0, src_hist_cells_size));

	// Calculate cells histograms
	load_kernel_rgb_basic << <cuda_grid_size, cuda_block_size >> > (dev_image, dev_src_hist_cells, dev_dst_hist_cells, cell_size, num_orientation_bins, gradient_type, image.nc(), cell_nc);
	CHECK(cudaDeviceSynchronize());

	// Copy result to host memory
	CHECK(cudaMemcpy(hist_cells, dev_dst_hist_cells, dst_hist_cells_size, cudaMemcpyDeviceToHost));

	CHECK(cudaFree(dev_image));
}


//*********************************************************************************************
// GRAYSCALE kernels
//*********************************************************************************************


__global__ void load_kernel_grayscale_basic(unsigned char *dev_image, double* src_hist_cells, double* dst_hist_cells, const unsigned long cell_size, const unsigned long num_orientation_bins, const int gradient_type, const unsigned long image_nc, const unsigned long cell_nc)
{

	// block row offset + block column offset
	unsigned int block_offset = blockIdx.x / cell_nc * cell_size * image_nc + blockIdx.x % cell_nc * cell_size;

	// thread row offset + thread column offset
	unsigned int thread_offset = threadIdx.x / cell_size * image_nc + threadIdx.x % cell_size;

	// +image_nc and +1 to keeped border of 1 pixel around the image (top and left)
	unsigned int pid = block_offset + thread_offset + image_nc + 1;

	// Read pixels around current position
	unsigned long left = dev_image[pid - 1];
	unsigned long right = dev_image[pid + 1];
	unsigned long top = dev_image[pid - image_nc];
	unsigned long bottom = dev_image[pid + image_nc];

	// Calculate x and y gradients
	double grad_x = (long)right - (long)left;
	double grad_y = (long)top - (long)bottom;

	// obtain the angle of the gradient.  Make sure it is scaled between 0 and 1.
	double angle = fmax(0.0, atan2(grad_y, grad_x) / CUDART_PI + 1) / 2;

	if (gradient_type == hog_unsigned_gradient)
	{
		angle *= 2;
		if (angle >= 1)
			angle -= 1;
	}

	// now scale angle to between 0 and num_orientation_bins
	angle *= num_orientation_bins;

	// Gradient magnitude
	const double strength = sqrt(grad_y*grad_y + grad_x * grad_x);

	// Local hog position (same round performed by DLIB)
	unsigned long round_to_int = static_cast<unsigned long>(floor(angle + 0.5));
	int local_hog_id = round_to_int % num_orientation_bins;

	// hog id is made by block offset + thread offset
	unsigned int hog_block_offset = blockIdx.x * cell_size * cell_size * num_orientation_bins;
	unsigned int hog_thread_offset = threadIdx.x * num_orientation_bins;

	// Store gradient magnitude on thread histogram
	src_hist_cells[hog_block_offset + hog_thread_offset + local_hog_id] = strength;

	// Parallel reduction on thread histogram in order to calculate cell histogram
	__syncthreads();

	for (int stride = blockDim.x / 2; stride > 0; stride /= 2)
	{
		if (threadIdx.x < stride)
		{
			for (int i = 0; i < num_orientation_bins; i++)
			{
				src_hist_cells[hog_block_offset + hog_thread_offset + i] += src_hist_cells[hog_block_offset + (stride + threadIdx.x) * num_orientation_bins + i];
			}
		}
		__syncthreads();
	}

	// Store histogram to destination array
	if (threadIdx.x == 0)
	{
		for (int i = 0; i < num_orientation_bins; i++)
		{
			dst_hist_cells[blockIdx.x * num_orientation_bins + i] = src_hist_cells[hog_block_offset + hog_thread_offset + i];
		}
	}
}

__global__ void load_kernel_grayscale_shared_hist(unsigned char *dev_image, double* dst_hist_cells, const unsigned long cell_size, const unsigned long num_orientation_bins, const int gradient_type, const unsigned long image_nc, const unsigned long cell_nc)
{
	// hog id is made by block offset + thread offset
	unsigned int hog_block_offset = 0; // no block offset for shared memory
	unsigned int hog_thread_offset = threadIdx.x * num_orientation_bins;

	// Initialize shared memory with 0 (histogram initial value)
	extern __shared__ double src_hist_cells[];
	for (int i = 0; i < num_orientation_bins; i++)
	{
		src_hist_cells[hog_block_offset + hog_thread_offset + i] = 0;
	}
	__syncthreads();

	// block row offset + block column offset
	unsigned int block_offset = blockIdx.x / cell_nc * cell_size * image_nc + blockIdx.x % cell_nc * cell_size;

	// thread row offset + thread column offset
	unsigned int thread_offset = threadIdx.x / cell_size * image_nc + threadIdx.x % cell_size;

	// +image_nc and +1 to keeped border of 1 pixel around the image (top and left)
	unsigned int pid = block_offset + thread_offset + image_nc + 1;

	// Read pixels around current position
	unsigned long left = dev_image[pid - 1];
	unsigned long right = dev_image[pid + 1];
	unsigned long top = dev_image[pid - image_nc];
	unsigned long bottom = dev_image[pid + image_nc];

	// Calculate x and y gradients
	double grad_x = (long)right - (long)left;
	double grad_y = (long)top - (long)bottom;

	// obtain the angle of the gradient.  Make sure it is scaled between 0 and 1.
	double angle = fmax(0.0, atan2(grad_y, grad_x) / CUDART_PI + 1) / 2;

	if (gradient_type == hog_unsigned_gradient)
	{
		angle *= 2;
		if (angle >= 1)
			angle -= 1;
	}

	// now scale angle to between 0 and num_orientation_bins
	angle *= num_orientation_bins;

	// Gradient magnitude
	const double strength = sqrt(grad_y*grad_y + grad_x * grad_x);

	// Local hog position (same round performed by DLIB)
	unsigned long round_to_int = static_cast<unsigned long>(floor(angle + 0.5));
	int local_hog_id = round_to_int % num_orientation_bins;

	// Store gradient magnitude on thread histogram
	src_hist_cells[hog_block_offset + hog_thread_offset + local_hog_id] = strength;

	// Parallel reduction on thread histogram in order to calculate cell histogram
	__syncthreads();

	for (int stride = blockDim.x / 2; stride > 0; stride /= 2)
	{
		if (threadIdx.x < stride)
		{
			for (int i = 0; i < num_orientation_bins; i++)
			{
				src_hist_cells[hog_block_offset + hog_thread_offset + i] += src_hist_cells[hog_block_offset + (stride + threadIdx.x) * num_orientation_bins + i];
			}
		}
		__syncthreads();
	}

	// Store histogram to destination array
	if (threadIdx.x == 0)
	{
		for (int i = 0; i < num_orientation_bins; i++)
		{
			dst_hist_cells[blockIdx.x * num_orientation_bins + i] = src_hist_cells[hog_block_offset + hog_thread_offset + i];
		}
	}
}

__global__ void load_kernel_grayscale_shared_hist_opt(unsigned char *dev_image, double* dst_hist_cells, const unsigned long cell_size, const unsigned long num_orientation_bins, const int gradient_type, const unsigned long image_nc, const unsigned long cell_nc)
{
	unsigned int warp_offset = num_orientation_bins * WARP_SIZE;

	// hog id is made by block offset + thread offset
	unsigned int hog_thread_offset = warp_offset * (threadIdx.x / WARP_SIZE) + threadIdx.x % WARP_SIZE;

	// Initialize shared memory with 0 (histogram initial value)
	extern __shared__ double src_hist_cells[];
	for (int i = 0; i < num_orientation_bins; i++)
	{
		src_hist_cells[hog_thread_offset + i * WARP_SIZE] = 0;
	}
	__syncthreads();

	// block row offset + block column offset
	unsigned int block_offset = blockIdx.x / cell_nc * cell_size * image_nc + blockIdx.x % cell_nc * cell_size;

	// thread row offset + thread column offset
	unsigned int thread_offset = threadIdx.x / cell_size * image_nc + threadIdx.x % cell_size;

	// +image_nc and +1 to keeped border of 1 pixel around the image (top and left)
	unsigned int pid = block_offset + thread_offset + image_nc + 1;

	// Read pixels around current position
	unsigned long left = dev_image[pid - 1];
	unsigned long right = dev_image[pid + 1];
	unsigned long top = dev_image[pid - image_nc];
	unsigned long bottom = dev_image[pid + image_nc];

	// Calculate x and y gradients
	double grad_x = (long)right - (long)left;
	double grad_y = (long)top - (long)bottom;

	// obtain the angle of the gradient.  Make sure it is scaled between 0 and 1.
	double angle = fmax(0.0, atan2(grad_y, grad_x) / CUDART_PI + 1) / 2;

	if (gradient_type == hog_unsigned_gradient)
	{
		angle *= 2;
		if (angle >= 1)
			angle -= 1;
	}

	// now scale angle to between 0 and num_orientation_bins
	angle *= num_orientation_bins;

	// Gradient magnitude
	const double strength = sqrt(grad_y*grad_y + grad_x * grad_x);

	// Local hog position (same round performed by DLIB)
	unsigned long round_to_int = static_cast<unsigned long>(floor(angle + 0.5));
	int local_hog_id = round_to_int % num_orientation_bins;

	// Store gradient magnitude on thread histogram
	src_hist_cells[hog_thread_offset + local_hog_id * WARP_SIZE] = strength;

	// Parallel reduction on thread histogram in order to calculate cell histogram
	__syncthreads();

	for (int stride = blockDim.x / 2; stride > 0; stride /= 2)
	{
		if (threadIdx.x < stride)
		{
			unsigned int hog_stride_thread_offset = warp_offset * ((stride + threadIdx.x) / WARP_SIZE) + (stride + threadIdx.x) % WARP_SIZE;
			for (int i = 0; i < num_orientation_bins; i++)
			{
				src_hist_cells[hog_thread_offset + i * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + i * WARP_SIZE];
			}
		}
		__syncthreads();
	}

	// Store histogram to destination array
	if (threadIdx.x == 0)
	{
		for (int i = 0; i < num_orientation_bins; i++)
		{
			dst_hist_cells[blockIdx.x * num_orientation_bins + i] = src_hist_cells[hog_thread_offset + i * WARP_SIZE];
		}
	}
}

__global__ void load_kernel_grayscale_9bins_shared_hist_opt(unsigned char *dev_image, double* dst_hist_cells, const unsigned long cell_size, const int gradient_type, const unsigned long image_nc, const unsigned long cell_nc)
{
	unsigned int warp_offset = _9BINS * WARP_SIZE;

	// hog id is made by block offset + thread offset
	unsigned int hog_thread_offset = warp_offset * (threadIdx.x / WARP_SIZE) + threadIdx.x % WARP_SIZE;

	// Initialize shared memory with 0 (histogram initial value)
	extern __shared__ double src_hist_cells[];
	src_hist_cells[hog_thread_offset + 0 * WARP_SIZE] = 0;
	src_hist_cells[hog_thread_offset + 1 * WARP_SIZE] = 0;
	src_hist_cells[hog_thread_offset + 2 * WARP_SIZE] = 0;
	src_hist_cells[hog_thread_offset + 3 * WARP_SIZE] = 0;
	src_hist_cells[hog_thread_offset + 4 * WARP_SIZE] = 0;
	src_hist_cells[hog_thread_offset + 5 * WARP_SIZE] = 0;
	src_hist_cells[hog_thread_offset + 6 * WARP_SIZE] = 0;
	src_hist_cells[hog_thread_offset + 7 * WARP_SIZE] = 0;
	src_hist_cells[hog_thread_offset + 8 * WARP_SIZE] = 0;

	__syncthreads();

	// block row offset + block column offset
	unsigned int block_offset = blockIdx.x / cell_nc * cell_size * image_nc + blockIdx.x % cell_nc * cell_size;

	// thread row offset + thread column offset
	unsigned int thread_offset = threadIdx.x / cell_size * image_nc + threadIdx.x % cell_size;

	// +image_nc and +1 to keeped border of 1 pixel around the image (top and left)
	unsigned int pid = block_offset + thread_offset + image_nc + 1;

	// Read pixels around current position
	unsigned long left = dev_image[pid - 1];
	unsigned long right = dev_image[pid + 1];
	unsigned long top = dev_image[pid - image_nc];
	unsigned long bottom = dev_image[pid + image_nc];

	// Calculate x and y gradients
	double grad_x = (long)right - (long)left;
	double grad_y = (long)top - (long)bottom;

	// obtain the angle of the gradient.  Make sure it is scaled between 0 and 1.
	double angle = fmax(0.0, atan2(grad_y, grad_x) / CUDART_PI + 1) / 2;

	if (gradient_type == hog_unsigned_gradient)
	{
		angle *= 2;
		if (angle >= 1)
			angle -= 1;
	}

	// now scale angle to between 0 and num_orientation_bins
	angle *= _9BINS;

	// Gradient magnitude
	const double strength = sqrt(grad_y*grad_y + grad_x * grad_x);

	// Local hog position (same round performed by DLIB)
	unsigned long round_to_int = static_cast<unsigned long>(floor(angle + 0.5));
	int local_hog_id = round_to_int % _9BINS;

	// Store gradient magnitude on thread histogram
	src_hist_cells[hog_thread_offset + local_hog_id * WARP_SIZE] = strength;

	// Parallel reduction on thread histogram in order to calculate cell histogram
	__syncthreads();

	for (int stride = blockDim.x / 2; stride > WARP_SIZE; stride /= 2)
	{
		if (threadIdx.x < stride)
		{
			unsigned int hog_stride_thread_offset = warp_offset * ((stride + threadIdx.x) / WARP_SIZE) + (stride + threadIdx.x) % WARP_SIZE;
			src_hist_cells[hog_thread_offset + 0 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 0 * WARP_SIZE];
			src_hist_cells[hog_thread_offset + 1 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 1 * WARP_SIZE];
			src_hist_cells[hog_thread_offset + 2 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 2 * WARP_SIZE];
			src_hist_cells[hog_thread_offset + 3 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 3 * WARP_SIZE];
			src_hist_cells[hog_thread_offset + 4 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 4 * WARP_SIZE];
			src_hist_cells[hog_thread_offset + 5 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 5 * WARP_SIZE];
			src_hist_cells[hog_thread_offset + 6 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 6 * WARP_SIZE];
			src_hist_cells[hog_thread_offset + 7 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 7 * WARP_SIZE];
			src_hist_cells[hog_thread_offset + 8 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 8 * WARP_SIZE];
		}
		__syncthreads();
	}

	// WARP Unrolling
	if (threadIdx.x < WARP_SIZE) {
		// 32
		unsigned int stride = 32;
		unsigned int hog_stride_thread_offset = warp_offset * ((stride + threadIdx.x) / WARP_SIZE) + (stride + threadIdx.x) % WARP_SIZE;
		src_hist_cells[hog_thread_offset + 0 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 0 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 1 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 1 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 2 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 2 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 3 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 3 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 4 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 4 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 5 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 5 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 6 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 6 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 7 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 7 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 8 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 8 * WARP_SIZE];
		// 16
		stride = 16;
		hog_stride_thread_offset = warp_offset * ((stride + threadIdx.x) / WARP_SIZE) + (stride + threadIdx.x) % WARP_SIZE;
		src_hist_cells[hog_thread_offset + 0 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 0 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 1 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 1 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 2 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 2 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 3 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 3 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 4 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 4 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 5 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 5 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 6 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 6 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 7 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 7 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 8 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 8 * WARP_SIZE];
		// 8
		stride = 8;
		hog_stride_thread_offset = warp_offset * ((stride + threadIdx.x) / WARP_SIZE) + (stride + threadIdx.x) % WARP_SIZE;
		src_hist_cells[hog_thread_offset + 0 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 0 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 1 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 1 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 2 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 2 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 3 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 3 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 4 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 4 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 5 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 5 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 6 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 6 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 7 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 7 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 8 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 8 * WARP_SIZE];
		// 4
		stride = 4;
		hog_stride_thread_offset = warp_offset * ((stride + threadIdx.x) / WARP_SIZE) + (stride + threadIdx.x) % WARP_SIZE;
		src_hist_cells[hog_thread_offset + 0 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 0 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 1 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 1 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 2 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 2 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 3 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 3 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 4 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 4 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 5 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 5 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 6 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 6 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 7 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 7 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 8 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 8 * WARP_SIZE];
		// 2
		stride = 2;
		hog_stride_thread_offset = warp_offset * ((stride + threadIdx.x) / WARP_SIZE) + (stride + threadIdx.x) % WARP_SIZE;
		src_hist_cells[hog_thread_offset + 0 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 0 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 1 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 1 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 2 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 2 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 3 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 3 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 4 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 4 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 5 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 5 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 6 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 6 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 7 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 7 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 8 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 8 * WARP_SIZE];
		// 1
		stride = 1;
		hog_stride_thread_offset = warp_offset * ((stride + threadIdx.x) / WARP_SIZE) + (stride + threadIdx.x) % WARP_SIZE;
		src_hist_cells[hog_thread_offset + 0 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 0 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 1 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 1 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 2 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 2 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 3 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 3 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 4 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 4 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 5 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 5 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 6 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 6 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 7 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 7 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 8 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 8 * WARP_SIZE];
	}

	// Store histogram to destination array
	if (threadIdx.x == 0)
	{
		dst_hist_cells[blockIdx.x * _9BINS + 0] = src_hist_cells[hog_thread_offset + 0 * WARP_SIZE];
		dst_hist_cells[blockIdx.x * _9BINS + 1] = src_hist_cells[hog_thread_offset + 1 * WARP_SIZE];
		dst_hist_cells[blockIdx.x * _9BINS + 2] = src_hist_cells[hog_thread_offset + 2 * WARP_SIZE];
		dst_hist_cells[blockIdx.x * _9BINS + 3] = src_hist_cells[hog_thread_offset + 3 * WARP_SIZE];
		dst_hist_cells[blockIdx.x * _9BINS + 4] = src_hist_cells[hog_thread_offset + 4 * WARP_SIZE];
		dst_hist_cells[blockIdx.x * _9BINS + 5] = src_hist_cells[hog_thread_offset + 5 * WARP_SIZE];
		dst_hist_cells[blockIdx.x * _9BINS + 6] = src_hist_cells[hog_thread_offset + 6 * WARP_SIZE];
		dst_hist_cells[blockIdx.x * _9BINS + 7] = src_hist_cells[hog_thread_offset + 7 * WARP_SIZE];
		dst_hist_cells[blockIdx.x * _9BINS + 8] = src_hist_cells[hog_thread_offset + 8 * WARP_SIZE];
	}
}

__global__ void load_kernel_grayscale_8cells_9bins_shared_hist_opt(unsigned char *dev_image, double* dst_hist_cells, const int gradient_type, const unsigned long image_nc, const unsigned long cell_nc)
{
	unsigned int warp_offset = _9BINS * WARP_SIZE;

	// hog id is made by block offset + thread offset
	unsigned int hog_thread_offset = warp_offset * (threadIdx.x / WARP_SIZE) + threadIdx.x % WARP_SIZE;

	// Initialize shared memory with 0 (histogram initial value)
	// Static shared memory allocation, 576 comes from 8 * 8 * 9 (= cell_size * cell_size * num bins)
	__shared__ double src_hist_cells[576];
	src_hist_cells[hog_thread_offset + 0 * WARP_SIZE] = 0;
	src_hist_cells[hog_thread_offset + 1 * WARP_SIZE] = 0;
	src_hist_cells[hog_thread_offset + 2 * WARP_SIZE] = 0;
	src_hist_cells[hog_thread_offset + 3 * WARP_SIZE] = 0;
	src_hist_cells[hog_thread_offset + 4 * WARP_SIZE] = 0;
	src_hist_cells[hog_thread_offset + 5 * WARP_SIZE] = 0;
	src_hist_cells[hog_thread_offset + 6 * WARP_SIZE] = 0;
	src_hist_cells[hog_thread_offset + 7 * WARP_SIZE] = 0;
	src_hist_cells[hog_thread_offset + 8 * WARP_SIZE] = 0;

	__syncthreads();

	// block row offset + block column offset
	unsigned int block_offset = blockIdx.x / cell_nc * _8CELLS * image_nc + blockIdx.x % cell_nc * _8CELLS;

	// thread row offset + thread column offset
	unsigned int thread_offset = threadIdx.x / _8CELLS * image_nc + threadIdx.x % _8CELLS;

	// +image_nc and +1 to keeped border of 1 pixel around the image (top and left)
	unsigned int pid = block_offset + thread_offset + image_nc + 1;

	// Read pixels around current position
	unsigned long left = dev_image[pid - 1];
	unsigned long right = dev_image[pid + 1];
	unsigned long top = dev_image[pid - image_nc];
	unsigned long bottom = dev_image[pid + image_nc];

	// Calculate x and y gradients
	double grad_x = (long)right - (long)left;
	double grad_y = (long)top - (long)bottom;

	// obtain the angle of the gradient.  Make sure it is scaled between 0 and 1.
	double angle = fmax(0.0, atan2(grad_y, grad_x) / CUDART_PI + 1) / 2;

	if (gradient_type == hog_unsigned_gradient)
	{
		angle *= 2;
		if (angle >= 1)
			angle -= 1;
	}

	// now scale angle to between 0 and num_orientation_bins
	angle *= _9BINS;

	// Gradient magnitude
	const double strength = sqrt(grad_y*grad_y + grad_x * grad_x);

	// Local hog position (same round performed by DLIB)
	unsigned long round_to_int = static_cast<unsigned long>(floor(angle + 0.5));
	int local_hog_id = round_to_int % _9BINS;

	// Store gradient magnitude on thread histogram
	src_hist_cells[hog_thread_offset + local_hog_id * WARP_SIZE] = strength;

	// Parallel reduction on thread histogram in order to calculate cell histogram
	__syncthreads();

	for (int stride = blockDim.x / 2; stride > WARP_SIZE; stride /= 2)
	{
		if (threadIdx.x < stride)
		{
			unsigned int hog_stride_thread_offset = warp_offset * ((stride + threadIdx.x) / WARP_SIZE) + (stride + threadIdx.x) % WARP_SIZE;
			src_hist_cells[hog_thread_offset + 0 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 0 * WARP_SIZE];
			src_hist_cells[hog_thread_offset + 1 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 1 * WARP_SIZE];
			src_hist_cells[hog_thread_offset + 2 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 2 * WARP_SIZE];
			src_hist_cells[hog_thread_offset + 3 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 3 * WARP_SIZE];
			src_hist_cells[hog_thread_offset + 4 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 4 * WARP_SIZE];
			src_hist_cells[hog_thread_offset + 5 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 5 * WARP_SIZE];
			src_hist_cells[hog_thread_offset + 6 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 6 * WARP_SIZE];
			src_hist_cells[hog_thread_offset + 7 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 7 * WARP_SIZE];
			src_hist_cells[hog_thread_offset + 8 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 8 * WARP_SIZE];
		}
		__syncthreads();
	}

	// WARP Unrolling
	if (threadIdx.x < WARP_SIZE) {
		// 32
		unsigned int stride = 32;
		unsigned int hog_stride_thread_offset = warp_offset * ((stride + threadIdx.x) / WARP_SIZE) + (stride + threadIdx.x) % WARP_SIZE;
		src_hist_cells[hog_thread_offset + 0 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 0 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 1 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 1 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 2 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 2 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 3 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 3 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 4 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 4 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 5 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 5 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 6 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 6 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 7 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 7 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 8 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 8 * WARP_SIZE];
		// 16
		stride = 16;
		hog_stride_thread_offset = warp_offset * ((stride + threadIdx.x) / WARP_SIZE) + (stride + threadIdx.x) % WARP_SIZE;
		src_hist_cells[hog_thread_offset + 0 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 0 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 1 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 1 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 2 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 2 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 3 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 3 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 4 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 4 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 5 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 5 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 6 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 6 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 7 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 7 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 8 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 8 * WARP_SIZE];
		// 8
		stride = 8;
		hog_stride_thread_offset = warp_offset * ((stride + threadIdx.x) / WARP_SIZE) + (stride + threadIdx.x) % WARP_SIZE;
		src_hist_cells[hog_thread_offset + 0 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 0 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 1 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 1 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 2 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 2 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 3 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 3 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 4 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 4 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 5 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 5 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 6 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 6 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 7 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 7 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 8 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 8 * WARP_SIZE];
		// 4
		stride = 4;
		hog_stride_thread_offset = warp_offset * ((stride + threadIdx.x) / WARP_SIZE) + (stride + threadIdx.x) % WARP_SIZE;
		src_hist_cells[hog_thread_offset + 0 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 0 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 1 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 1 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 2 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 2 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 3 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 3 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 4 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 4 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 5 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 5 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 6 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 6 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 7 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 7 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 8 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 8 * WARP_SIZE];
		// 2
		stride = 2;
		hog_stride_thread_offset = warp_offset * ((stride + threadIdx.x) / WARP_SIZE) + (stride + threadIdx.x) % WARP_SIZE;
		src_hist_cells[hog_thread_offset + 0 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 0 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 1 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 1 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 2 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 2 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 3 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 3 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 4 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 4 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 5 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 5 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 6 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 6 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 7 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 7 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 8 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 8 * WARP_SIZE];
		// 1
		stride = 1;
		hog_stride_thread_offset = warp_offset * ((stride + threadIdx.x) / WARP_SIZE) + (stride + threadIdx.x) % WARP_SIZE;
		src_hist_cells[hog_thread_offset + 0 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 0 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 1 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 1 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 2 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 2 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 3 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 3 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 4 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 4 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 5 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 5 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 6 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 6 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 7 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 7 * WARP_SIZE];
		src_hist_cells[hog_thread_offset + 8 * WARP_SIZE] += src_hist_cells[hog_stride_thread_offset + 8 * WARP_SIZE];
	}

	// Store histogram to destination array
	if (threadIdx.x == 0)
	{
		dst_hist_cells[blockIdx.x * _9BINS + 0] = src_hist_cells[hog_thread_offset + 0 * WARP_SIZE];
		dst_hist_cells[blockIdx.x * _9BINS + 1] = src_hist_cells[hog_thread_offset + 1 * WARP_SIZE];
		dst_hist_cells[blockIdx.x * _9BINS + 2] = src_hist_cells[hog_thread_offset + 2 * WARP_SIZE];
		dst_hist_cells[blockIdx.x * _9BINS + 3] = src_hist_cells[hog_thread_offset + 3 * WARP_SIZE];
		dst_hist_cells[blockIdx.x * _9BINS + 4] = src_hist_cells[hog_thread_offset + 4 * WARP_SIZE];
		dst_hist_cells[blockIdx.x * _9BINS + 5] = src_hist_cells[hog_thread_offset + 5 * WARP_SIZE];
		dst_hist_cells[blockIdx.x * _9BINS + 6] = src_hist_cells[hog_thread_offset + 6 * WARP_SIZE];
		dst_hist_cells[blockIdx.x * _9BINS + 7] = src_hist_cells[hog_thread_offset + 7 * WARP_SIZE];
		dst_hist_cells[blockIdx.x * _9BINS + 8] = src_hist_cells[hog_thread_offset + 8 * WARP_SIZE];
	}
}

__global__ void load_kernel_grayscale_shared_img(unsigned char *dev_image, double* src_hist_cells, double* dst_hist_cells, const unsigned long cell_size, const unsigned long num_orientation_bins, const int gradient_type, const unsigned long image_nc, const unsigned long cell_nc)
{
	// block row offset + block column offset
	unsigned int block_offset = blockIdx.x / cell_nc * cell_size * image_nc + blockIdx.x % cell_nc * cell_size;

	// thread row offset + thread column offset
	unsigned int thread_offset = threadIdx.x / cell_size * image_nc + threadIdx.x % cell_size;

	// +image_nc and +1 to keeped border of 1 pixel around the image (top and left)
	unsigned int pid = block_offset + thread_offset + image_nc + 1;

	unsigned int shared_nc = cell_size + 2;
	unsigned int shared_pid = shared_nc * (1 + threadIdx.x / cell_size) + threadIdx.x % cell_size + 1;

	extern __shared__ unsigned char dev_image_shared[];

	// Central pixel
	dev_image_shared[shared_pid] = dev_image[pid];

	// First row, up pixel
	if (threadIdx.x < cell_size)
	{
		dev_image_shared[shared_pid - cell_size - 2] = dev_image[pid - image_nc];
	}
	// Last row, down pixel
	else if (threadIdx.x >= blockDim.x - cell_size)
	{
		dev_image_shared[shared_pid + cell_size + 2] = dev_image[pid + image_nc];
	}
	// Left column, left pixel
	if (threadIdx.x % cell_size == 0)
	{
		dev_image_shared[shared_pid - 1] = dev_image[pid - 1];
	}
	// Right, right pixel
	else if (threadIdx.x % cell_size == cell_size - 1)
	{
		dev_image_shared[shared_pid + 1] = dev_image[pid + 1];
	}
	__syncthreads();


	// Read pixels around current position
	unsigned long left = dev_image_shared[shared_pid - 1];
	unsigned long right = dev_image_shared[shared_pid + 1];
	unsigned long top = dev_image_shared[shared_pid - cell_size - 2];
	unsigned long bottom = dev_image_shared[shared_pid + cell_size + 2];

	// Calculate x and y gradients
	double grad_x = (long)right - (long)left;
	double grad_y = (long)top - (long)bottom;

	// obtain the angle of the gradient.  Make sure it is scaled between 0 and 1.
	double angle = fmax(0.0, atan2(grad_y, grad_x) / CUDART_PI + 1) / 2;

	if (gradient_type == hog_unsigned_gradient)
	{
		angle *= 2;
		if (angle >= 1)
			angle -= 1;
	}

	// now scale angle to between 0 and num_orientation_bins
	angle *= num_orientation_bins;

	// Gradient magnitude
	const double strength = sqrt(grad_y*grad_y + grad_x * grad_x);

	// Local hog position (same round performed by DLIB)
	unsigned long round_to_int = static_cast<unsigned long>(floor(angle + 0.5));
	int local_hog_id = round_to_int % num_orientation_bins;

	// hog id is made by block offset + thread offset
	unsigned int hog_block_offset = blockIdx.x * cell_size * cell_size * num_orientation_bins;
	unsigned int hog_thread_offset = threadIdx.x * num_orientation_bins;

	// Store gradient magnitude on thread histogram
	src_hist_cells[hog_block_offset + hog_thread_offset + local_hog_id] = strength;

	// Parallel reduction on thread histogram in order to calculate cell histogram
	__syncthreads();

	for (int stride = blockDim.x / 2; stride > 0; stride /= 2)
	{
		if (threadIdx.x < stride)
		{
			for (int i = 0; i < num_orientation_bins; i++)
			{
				src_hist_cells[hog_block_offset + hog_thread_offset + i] += src_hist_cells[hog_block_offset + (stride + threadIdx.x) * num_orientation_bins + i];
			}
		}
		__syncthreads();
	}

	// Store histogram to destination array
	if (threadIdx.x == 0)
	{
		for (int i = 0; i < num_orientation_bins; i++)
		{
			dst_hist_cells[blockIdx.x * num_orientation_bins + i] = src_hist_cells[hog_block_offset + hog_thread_offset + i];
		}
	}
}


//*********************************************************************************************
// RGB_PIXEL kernels
//*********************************************************************************************

__device__ void assign_pixel_gpu(unsigned long& dst, rgb_pixel& src)
{
	dst = (src.blue + src.green + src.red) / 3;
}


__global__ void load_kernel_rgb_basic(rgb_pixel *dev_image, double* src_hist_cells, double* dst_hist_cells, const unsigned long cell_size, const unsigned long num_orientation_bins, const int gradient_type, const unsigned long image_nc, const unsigned long cell_nc)
{
	// block row offset + block column offset
	unsigned int block_offset = blockIdx.x / cell_nc * cell_size * image_nc + blockIdx.x % cell_nc * cell_size;

	// thread row offset + thread column offset
	unsigned int thread_offset = threadIdx.x / cell_size * image_nc + threadIdx.x % cell_size;

	// +image_nc and +1 to keeped border of 1 pixel around the image (top and left)
	unsigned int pid = block_offset + thread_offset + image_nc + 1;

	// Read pixels around current position
	unsigned long left;
	unsigned long right;
	unsigned long top;
	unsigned long bottom;
	assign_pixel_gpu(left, dev_image[pid - 1]);
	assign_pixel_gpu(right, dev_image[pid + 1]);
	assign_pixel_gpu(top, dev_image[pid - image_nc]);
	assign_pixel_gpu(bottom, dev_image[pid + image_nc]);

	// Calculate x and y gradients
	double grad_x = (long)right - (long)left;
	double grad_y = (long)top - (long)bottom;

	// obtain the angle of the gradient.  Make sure it is scaled between 0 and 1.
	double angle = fmax(0.0, atan2(grad_y, grad_x) / CUDART_PI + 1) / 2;

	if (gradient_type == hog_unsigned_gradient)
	{
		angle *= 2;
		if (angle >= 1)
			angle -= 1;
	}

	// now scale angle to between 0 and num_orientation_bins
	angle *= num_orientation_bins;

	// Gradient magnitude
	const double strength = sqrt(grad_y*grad_y + grad_x * grad_x);

	// Local hog position (same round performed by DLIB)
	unsigned long round_to_int = static_cast<unsigned long>(floor(angle + 0.5));
	int local_hog_id = round_to_int % num_orientation_bins;

	// hog id is made by block offset + thread offset
	unsigned int hog_block_offset = blockIdx.x * cell_size * cell_size * num_orientation_bins;
	unsigned int hog_thread_offset = threadIdx.x * num_orientation_bins;

	// Store gradient magnitude on thread histogram
	src_hist_cells[hog_block_offset + hog_thread_offset + local_hog_id] = strength;

	// Parallel reduction on thread histogram in order to calculate cell histogram
	__syncthreads();

	for (int stride = blockDim.x / 2; stride > 0; stride /= 2)
	{
		if (threadIdx.x < stride)
		{
			for (int i = 0; i < num_orientation_bins; i++)
			{
				src_hist_cells[hog_block_offset + hog_thread_offset + i] += src_hist_cells[hog_block_offset + (stride + threadIdx.x) * num_orientation_bins + i];
			}
		}
		__syncthreads();
	}

	// Store histogram to destination array
	if (threadIdx.x == 0)
	{
		for (int i = 0; i < num_orientation_bins; i++)
		{
			dst_hist_cells[blockIdx.x * num_orientation_bins + i] = src_hist_cells[hog_block_offset + hog_thread_offset + i];
		}
	}
}
