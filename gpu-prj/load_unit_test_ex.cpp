#include <stdio.h>

#include "common.h"
#include "cuda_common.cu"
#include "hog_image_gpu.h"

#include <dlib/image_keypoint.h>
#include <dlib/image_transforms.h>
#include <dlib/array2d.h>
#include <dlib/pixel.h>

// *************************************
// Configurations

#define TEST_NUM 6					// number of test, on each test previous resolution is doubled
#define IMAGE_NR 128 				// 128 image starting number pixels per row
#define IMAGE_NC 64					// 64 image starting number of pixels per column
#define CELL_SIZE 8					// number of pixels of cell side 
#define BLOCK_SIZE 1				// block size, in number of cells
#define STRIDE 1					// stride size, in number of cells
#define NBINS 9						// number of orientation bins
#define GRADIENT_TYPE	hog_signed_gradient				// hog_signed_gradient, hog_unsigned_gradient, kernel_mode_shared_hist_opt
#define KERNEL_MODE		kernel_mode_shared_hist_opt		// kernel_mode_basic, kernel_mode_shared_hist, kernel_mode_shared_hist_opt, kernel_mode_9bins_shared_hist_opt, kernel_mode_8cells_9bins_shared_hist_opt, kernel_mode_shared_img

// *************************************


using namespace dlib;
using namespace std;

void check_results(hog_image<CELL_SIZE, BLOCK_SIZE, STRIDE, NBINS, GRADIENT_TYPE, hog_no_interpolation>& hog_cpu, hog_image_gpu<CELL_SIZE, BLOCK_SIZE, STRIDE, NBINS, GRADIENT_TYPE, hog_no_interpolation>& hog_gpu);

int main()
{ 
	// First cuda call is slow, run warmup to skip delay problems on next gpu kernel invocations
	device_warmup();

	// Seed for random numbers
	srand(time(NULL));

	// Run CPU vs GPU comparison against different image size (GRAYSCALE images)
	printf(".:: Grayscale ::.\n");
	for (int img_nr = IMAGE_NR, img_nc = IMAGE_NC, i = 0; i < TEST_NUM; img_nr *= 2, img_nc *= 2, i++)
	{
		array2d<unsigned char> img_grayscale;
		img_grayscale.set_size(img_nr, img_nc);

		// Setup the image with random pixel values
		for (int i = 0, v = 0; i < img_grayscale.nr(); i++)
		{
			for (int j = 0; j < img_grayscale.nc(); j++, v++)
			{
				img_grayscale[i][j] = (unsigned char)std::rand();
			}
		}

		// CPU hog
		auto start = current_time();
		hog_image<CELL_SIZE, BLOCK_SIZE, STRIDE, NBINS, GRADIENT_TYPE, hog_no_interpolation> hog_cpu;
		hog_cpu.load(img_grayscale);
		double cpu_time = elapsed_time(start);

		// GPU hog
		start = current_time();
		hog_image_gpu<CELL_SIZE, BLOCK_SIZE, STRIDE, NBINS, GRADIENT_TYPE, hog_no_interpolation> hog_gpu;
		hog_gpu.load(img_grayscale, KERNEL_MODE);
		double gpu_time = elapsed_time(start);
		printf("%d;%d;%f;%f;%.2f\n", img_nr, img_nc, cpu_time, gpu_time, cpu_time / gpu_time);

		check_results(hog_cpu, hog_gpu);
	}

	// Run CPU vs GPU comparison against different image size (RGB images)
	printf("\n.:: RGB ::.\n");
	for (int img_nr = IMAGE_NR, img_nc = IMAGE_NC, i = 0; i < TEST_NUM; img_nr *= 2, img_nc *= 2, i++)
	{
		array2d<rgb_pixel> img_rgb;
		img_rgb.set_size(img_nr, img_nc);

		// Setup the image with random pixel values
		for (int i = 0, v = 0; i < img_rgb.nr(); i++)
		{
			for (int j = 0; j < img_rgb.nc(); j++, v++)
			{
				rgb_pixel color((unsigned char)std::rand(), (unsigned char)std::rand(), (unsigned char)std::rand());
				img_rgb[i][j] = color;
			}
		}

		// CPU hog
		auto start = current_time();
		hog_image<CELL_SIZE, BLOCK_SIZE, STRIDE, NBINS, GRADIENT_TYPE, hog_no_interpolation> hog_cpu;
		hog_cpu.load(img_rgb);
		double cpu_time = elapsed_time(start);

		// GPU hog
		start = current_time();
		hog_image_gpu<CELL_SIZE, BLOCK_SIZE, STRIDE, NBINS, GRADIENT_TYPE, hog_no_interpolation> hog_gpu;
		hog_gpu.load(img_rgb);
		double gpu_time = elapsed_time(start);
		printf("%d;%d;%f;%f;%.2f\n", img_nr, img_nc, cpu_time, gpu_time, cpu_time / gpu_time);

		check_results(hog_cpu, hog_gpu);
	}

	return 0;
}

void check_results(hog_image<CELL_SIZE, BLOCK_SIZE, STRIDE, NBINS, GRADIENT_TYPE, hog_no_interpolation>& hog_cpu, hog_image_gpu<CELL_SIZE, BLOCK_SIZE, STRIDE, NBINS, GRADIENT_TYPE, hog_no_interpolation>& hog_gpu)
{
	// hog number of rows and columns
	if (hog_cpu.nr() != hog_gpu.nr())
	{
		printf("ERROR nr not equal\n");
		exit(1);
	}
	if (hog_cpu.nc() != hog_gpu.nc())
	{
		printf("ERROR nr not equal\n");
		exit(1);
	}

	// hog values
	int errors = 0;
	hog_image<CELL_SIZE, BLOCK_SIZE, STRIDE, NBINS, GRADIENT_TYPE, hog_no_interpolation>::descriptor_type cpu_operator;
	hog_image_gpu < CELL_SIZE, BLOCK_SIZE, STRIDE, NBINS, GRADIENT_TYPE, hog_no_interpolation>::descriptor_type gpu_operator;
	for (int i = 0; i < hog_cpu.nr(); i++)
	{
		for (int j = 0; j < hog_cpu.nc(); j++)
		{
			cpu_operator = hog_cpu.operator()(i, j);
			gpu_operator = hog_gpu.operator()(i, j);
			for (int k = 0; k < hog_cpu.get_num_dimensions(); k++)
			{
				double cpu_op = cpu_operator(k, 0);
				double gpu_op = gpu_operator(k, 0);
				if (!double_equals(cpu_op, gpu_op))
				{
					printf("operator[%2d][%2d][%2d] Comparing %.21lf with %.21lf ERROR\n", i, j, k, cpu_op, gpu_op);
					errors += 1;
				}
			}
		}
	}

	if (errors != 0)
	{
		printf("\nTEST ERROR, numer of errors %d\n", errors);
		exit(1);
	}
}