#pragma once
#include <stdio.h>
#include <dlib/image_keypoint.h>

using namespace dlib;

void load_image_gpu(const array2d<unsigned char>& img, unsigned long cell_size, const unsigned long num_orientation_bins, const int gradient_type, const unsigned int cell_nr, const unsigned int cell_nc, double* hist_cells, int kernel_mode);
void load_image_gpu(const array2d<rgb_pixel>& img, unsigned long cell_size, const unsigned long num_orientation_bins, const int gradient_type, const unsigned int cell_nr, const unsigned int cell_nc, double* hist_cells, int kernel_mode);

/*
	Kernel mode
*/
enum
{
	// Basic, no optimizations
	kernel_mode_basic,
	// Histograms are handled via shared memory
	kernel_mode_shared_hist,
	// As previuos kernel but shared memory indexing is optimized to remove bank conflicts
	kernel_mode_shared_hist_opt,
	// As previous but with number of orientation bins fixed to 9; applied warp unrolling 
	kernel_mode_9bins_shared_hist_opt,
	// As previous but with static shared memory
	kernel_mode_8cells_9bins_shared_hist_opt,
	// Image patch is handled via shared memory
	kernel_mode_shared_img
};

template <
	unsigned long cell_size_,
	unsigned long block_size_,
	unsigned long cell_stride_,
	unsigned long num_orientation_bins_,
	int           gradient_type_,
	int           interpolation_type_
>
class hog_image_gpu : public hog_image<cell_size_, block_size_, cell_stride_, num_orientation_bins_, gradient_type_, interpolation_type_>
{
public:

	hog_image_gpu()
	{
	}

	~hog_image_gpu()
	{
	}

	const static long min_size = cell_size * block_size + 2;
	
	/*
		GRAYSCALE image feature extractor
	*/
	void load(const array2d<unsigned char>& img, int kernel_mode=kernel_mode_basic)
	{
		// check if the window is just too small
		if (img.nr() < min_size || img.nc() < min_size)
		{
			// If the image is smaller than our windows then there aren't any descriptors at all!
			num_block_rows = 0;
			num_block_cols = 0;
			return;
		}

		// Calculate the number of cells depending on specified cell size
		unsigned int cell_nr;
		unsigned int cell_nc;
		init_hist_cells(img.nr(), img.nc(), cell_nr, cell_nc);

		// Invoke HOG descriptor extractor
		load_image_gpu(img, cell_size, num_orientation_bins, gradient_type, cell_nr, cell_nc, hist_cells, kernel_mode);

		// Init the number of blocks depending on the number of cells
		init_block_num(cell_nr, cell_nc);		
	}

	/*
		RGB image feature extractor
	*/
	void load(const array2d<rgb_pixel>& img, int kernel_mode = kernel_mode_basic)
	{
		// check if the window is just too small
		if (img.nr() < min_size || img.nc() < min_size)
		{
			// If the image is smaller than our windows then there aren't any descriptors at all!
			num_block_rows = 0;
			num_block_cols = 0;
			return;
		}

		// Calculate the number of cells depending on specified cell size
		unsigned int cell_nr;
		unsigned int cell_nc;
		init_hist_cells(img.nr(), img.nc(), cell_nr, cell_nc);

		// Invoke HOG descriptor extractor
		load_image_gpu(img, cell_size, num_orientation_bins, gradient_type, cell_nr, cell_nc, hist_cells, kernel_mode);

		// Init the number of blocks depending on the number of cells
		init_block_num(cell_nr, cell_nc);
	}

	/*
		Reset current object and free resources
	*/
	void clear()
	{
		num_block_rows = 0;
		num_block_cols = 0;
		free(hist_cells);		
	}

	/*
		Reset current object and free resources
	*/
	inline void unload() {
		clear();
	}

	/*
		Number of blocks
	*/
	inline size_t size(
	) const {
		return static_cast<size_t>(nr()*nc());
	}

	/*
		Number of block rows
	*/
	inline long nr(
	) const {
		return num_block_rows;
	}

	/*
		Number of block columns
	*/
	inline long nc(
	) const {
		return num_block_cols;
	}

	/*
		Size of the feature vector of one block
	*/
	long get_num_dimensions(
	) const
	{
		return block_size * block_size * num_orientation_bins;
	}

	/*
		Return the feature vector of the block at specified position
	*/
	inline const descriptor_type& operator() (
		long row,
		long col
		) const
	{
		// make sure requires clause is not broken
		DLIB_ASSERT(0 <= row && row < nr() &&
			0 <= col && col < nc(),
			"\t descriptor_type hog_image::operator()()"
			<< "\n\t invalid row or col argument"
			<< "\n\t row:  " << row
			<< "\n\t col:  " << col
			<< "\n\t nr(): " << nr()
			<< "\n\t nc(): " << nc()
			<< "\n\t this: " << this
		);

		unsigned long row_offset = num_cell_cols * num_orientation_bins;

		// Starting block position
		row *= cell_stride * row_offset;
		col *= cell_stride * num_orientation_bins;

		// Loop over block cells and concatenate cells feature vectors
		int feat = 0;
		for (unsigned long r = 0; r < block_size; ++r)
		{
			unsigned long roff = row + r * row_offset;
			for (unsigned long c = 0; c < block_size; ++c)
			{
				unsigned long coff = col + c * num_orientation_bins;
				for (unsigned long i = 0; i < num_orientation_bins; ++i)
				{
					des(feat++) = hist_cells[roff + coff + i];
				}
			}
		}
		des /= length(des) + 1e-8;
		return des;
	}

	/*
	 From Dlib doc:
	 returns a rectangle that tells you what part of the original image is associated with a particular HOG block.
	*/
	const rectangle get_block_rect(
		long row,
		long col
	) const
	{
		row *= cell_stride;
		col *= cell_stride;

		row *= cell_size;
		col *= cell_size;

		// do this to account for the 1 pixel padding we use all around the image
		++row;
		++col;

		return rectangle(col, row, col + cell_size * block_size - 1, row + cell_size * block_size - 1);
	}

	/* 
	 From Dlib doc:
	 Each local feature is extracted from a certain point in the input image.
                  This function returns the identity of the local feature corresponding
                  to the image location p.  Or in other words, let P == image_to_feat_space(p), 
                  then (*this)(P.y(),P.x()) == the local feature closest to, or centered at, 
                  the point p in the input image.  Note that some image points might not have 
                  corresponding feature locations.  E.g. border points or points outside the 
                  image.  In these cases the returned point will be outside get_rect(*this).
	*/
	const point image_to_feat_space(
		const point& p
	) const
	{

		const long half_block = block_size / 2;
		if ((block_size % 2) == 0)
		{
			return point(((p.x() - 1) / (long)cell_size - half_block) / (long)cell_stride,
				((p.y() - 1) / (long)cell_size - half_block) / (long)cell_stride);
		}
		else
		{
			return point(((p.x() - 1 - (long)cell_size / 2) / (long)cell_size - half_block) / (long)cell_stride,
				((p.y() - 1 - (long)cell_size / 2) / (long)cell_size - half_block) / (long)cell_stride);
		}
	}

	/*
	 From Dlib doc:
	 Returns the location in the input image space corresponding to the center
                  of the local feature at point p.  In other words, this function computes
                  the inverse of image_to_feat_space().  Note that it may only do so approximately, 
                  since more than one image location might correspond to the same local feature.  
                  That is, image_to_feat_space() might not be invertible so this function gives 
                  the closest possible result.
	*/
	const rectangle image_to_feat_space(
		const rectangle& rect
	) const
	{
		return rectangle(image_to_feat_space(rect.tl_corner()), image_to_feat_space(rect.br_corner()));
	}

	/*
	 From Dlib doc:
	 return rectangle(feat_to_image_space(rect.tl_corner()), feat_to_image_space(rect.br_corner()));
                  (i.e. maps a rectangle from feature space to image space)
	*/
	const point feat_to_image_space(
		const point& p
	) const
	{
		const long half_block = block_size / 2;
		if ((block_size % 2) == 0)
		{
			return point((p.x()*cell_stride + half_block)*cell_size + 1,
				(p.y()*cell_stride + half_block)*cell_size + 1);
		}
		else
		{
			return point((p.x()*cell_stride + half_block)*cell_size + 1 + cell_size / 2,
				(p.y()*cell_stride + half_block)*cell_size + 1 + cell_size / 2);
		}
	}

	const rectangle feat_to_image_space(
		const rectangle& rect
	) const
	{
		return rectangle(feat_to_image_space(rect.tl_corner()), feat_to_image_space(rect.br_corner()));
	}

private:
	
	double* hist_cells = NULL;

	mutable descriptor_type des;
	unsigned long num_block_rows;
	unsigned long num_block_cols;
	unsigned long num_cell_rows;
	unsigned long num_cell_cols;


	void init_hist_cells(unsigned int img_nr, unsigned int img_nc, unsigned int& cell_nr, unsigned int& cell_nc)
	{
		// As in original DLIB code we keep a border of 1 pixel all around the image so that we don't have
        // to worry about running outside the image when computing the horizontal and vertical gradients.
        
		// On the other side we don't use a border of unused cells

		cell_nr = (img_nr - 2) / cell_size;
		cell_nc = (img_nc - 2) / cell_size;
		hist_cells = (double*) malloc(cell_nr * cell_nc * num_orientation_bins * sizeof(double));
	}

	void init_block_num(unsigned int cell_nr, unsigned int cell_nc)
	{
		// Now figure out how many blocks we should have.  
		num_block_rows = (cell_nr - (block_size - 1) + cell_stride - 1) / cell_stride;
		num_block_cols = (cell_nc - (block_size - 1) + cell_stride - 1) / cell_stride;
		num_cell_rows = cell_nr;
		num_cell_cols = cell_nc;
	}

};

